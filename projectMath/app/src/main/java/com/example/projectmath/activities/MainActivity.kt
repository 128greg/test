package com.example.projectmath.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.projectmath.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    fun suma (view: View){
       var count1: Int = Integer.parseInt(editText2.text.toString())
       var count2: Int = Integer.parseInt(editText3.text.toString())
       var count3: Int = count1 + count2
      textView3.text = count3.toString()
    }
    fun riznica (view: View){
        var count1: Int = Integer.parseInt(editText2.text.toString())
        var count2: Int = Integer.parseInt(editText3.text.toString())
        var count3: Int = count1 - count2
        textView3.text = count3.toString()

        
    }
    fun dobutok (view: View){
        var count1: Int = Integer.parseInt(editText2.text.toString())
        var count2: Int = Integer.parseInt(editText3.text.toString())
        var count3: Int = count1 * count2
        textView3.text = count3.toString()
    }
    fun chastka (view: View){
        var count1 = editText2.text.toString().toFloat()
        var count2 = editText3.text.toString().toFloat()
        var count4 = count1 / count2
        textView3.text = count4.toString()
    }
}
